#! /usr/bin/env groovy

def call() {
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
//        sh 'git config --global user.email "jenkins@example.com"'
//        sh 'git config --global user.name "jenkins"'
//
//        sh 'git status'
//        sh 'git branch'
//        sh 'git config --list'

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-bootcamp-projects/java-maven-app.git"
        sh 'git add .'
        sh 'git commit -m "ci: version bump"'
        sh 'git push -u origin HEAD:feature/jenkins-shared-lib'
    }
}
