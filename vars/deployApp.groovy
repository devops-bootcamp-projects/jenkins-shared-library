#! /usr/bin/env groovy

def call(String imageName) {
    echo "Deploying the application to EC2..."
    //def dockerCmd = "docker run -p 8081:8080 -d $imageName"
    //def dockerComposeCmd = "docker-compose -f docker-compose.yaml up --detach"
    def shellCmd = "bash ./server-cmds.sh $imageName"
    sshagent(['ec2-server-key']) {
        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ec2-user@18.234.143.8:/home/ec2-user"
        sh "scp docker-compose.yaml ec2-user@18.234.143.8:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ec2-user@18.234.143.8 ${shellCmd}"
    }
}
